Exam Tools by jambl3r

To the extent possible under law, jambl3r, hereafter 'the publisher',
has waived all copyright and related or neighboring rights
to Exam Tools (hereafter 'the software').

This software is provided with absolutely no warranty. The publisher
is not responsible for any failures of the software, unexpected or
erronous output, or consequences of misuse of the software.

For the avoidance of doubt 'misuse' shall include using the software in
an academic exercise, examination or submission set by an institution
where the use of such software is disallowed by the institution.
